# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class SayEvent(Event3):
    NAME = "say"

    def perform(self):
        self.add_prop("said-"+self.object2)
        self.inform("say")
